#include <stdio.h>
#include "Hardmaze.h"

int w, a, s, d;
int count = 0;
char command;
int portal;
int cheat = 27023029;

int main()
{
	printf("Welcome to LabRat Industries.\n"); // introduction
	printf("In this experiment, you will be tested on your maze-solving skills.\n");
	printf("The number of moves you make will be recorded and you will pass/fail depending on the result. No pressure :)\n"); // instructions
	printf("The controls are simple.\nType the letter 'w' to move up, 'a' to move left, 's' to move down and 'd' to move right.\n");
	printf("Then hit enter to submit your choice.\n");
	printf("\nBefore we begin, if you know the secret cheatcode, please enter it below. If not, type in the the number '24732'\n");
	scanf("%d", &portal);
	if (portal == cheat)
		printf("Cheatcode confirmed.\n");
	printf("Good luck and remember, every puzzle has an answer.\n");

	HardMaze();

	return 0;
}

void HardMaze()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /         *   /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze1();
	}

}

void HardMaze1()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /         *                       \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'w' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze3();
	}
	else
		if (command == 'w')
		{
			count++;
			HardMaze();
		}
		else 
			if (command == 'd')
			{
				count++;
				HardMaze4();
			}

}

void HardMaze2()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                       *         \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze4();
	}
	else 
		if (command == 'd')
		{
			count++;
			HardMaze5();
		}

}

void HardMaze3()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /         *   /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd' && command != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze6();
	}
	else
		if (command == 'd')
		{
			count++;
			HardMaze1();
		}
		else
			if (command == 's')
			{
				count++;
				HardMaze10();
			}

}

void HardMaze4()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\   *   /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze1();
	}
	else
		if (command == 'd')
		{
			count++;
			HardMaze2();
		}

}

void HardMaze5()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\   *   /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze2();
	}
	else
		if (command == 's')
		{
			count++;
			HardMaze12();
		}

}

void HardMaze6()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\   *   /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'd')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze3();
	}

}

void HardMaze7()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /         *   /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze10();
	}
	else
		if (command == 's')
		{
			count++;
			HardMaze14();
		}

}

void HardMaze8()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\   *   /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 's')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze15();
	}

}

void HardMaze9()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\   *   /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 's')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze16();
	}

}

void HardMaze10()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\   *   /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'w' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'w')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze3();
	}
	else
		if (command == 'd')
		{
			count++;
			HardMaze7();
		}

}

void HardMaze11()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\   *         \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n         ");
	printf("        \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Right\n"); // states clearly the direction options
	if (portal == 27023029)
		printf("You could also try moving down.\n");
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	if (portal == 27023029)
	{
		while (command != 's' && command != 'd') // error identifier for directions. only allows 'a' or 's' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			scanf("%s", &command);
		}
		if (command == 's')// if user has typed in 'd', moves * to the right
		{
			count++;
			HardMaze36();
		}
		else
			if (command == 'd')
			{
				count++;
				HardMaze15();
			}
	}
	else
	{
		while (command != 'd') // error identifier for directions. only allows 'd' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			printf("In which direction would you like to go?\n");
			scanf("%s", &command);
		}

		if (command == 'd')// if user has typed in 'd', moves * to the right
		{
			count++;
			HardMaze15();
		}
	}
}

void HardMaze12()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\   *   /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 's')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze19();
	}
	else
		if (command == 'w')
		{
			count++;
			HardMaze5();
		}

}

void HardMaze13()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\   *                 /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 's')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze20();
	}
	else
		if (command == 'd')
		{
			count++;
			HardMaze17();
		}

}

void HardMaze14()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                 *   /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze17();
	}
	else
		if (command == 'w')
		{
			count++;
			HardMaze7();
		}

}

void HardMaze15()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\   *                 /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd' && command != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze11();
	}
	else
		if (command == 'd')
		{
			count++;
			HardMaze19();
		}
		else 
			if (command == 'w')
			{
				count++;
				HardMaze12();
			}

}

void HardMaze16()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                 *   /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze19();
	}
	else
		if (command == 'w')
		{
			count++;
			HardMaze9();
		}

}

void HardMaze17()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\   *   /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd' && command != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze13();
	}
	else
		if (command == 'd')
		{
			count++;
			HardMaze14();
		}
		else 
			if (command == 's')
			{
				count++;
				HardMaze24();
			}

}

void HardMaze18()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /         *   /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze21();
	}
	else
		if (command == 's')
		{
			count++;
			HardMaze25();
		}

}

void HardMaze19()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\   *   /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd' && command != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze15();
	}
	else
		if (command == 'd')
		{
			count++;
			HardMaze16();
		}
		else 
			if (command == 'w')
			{
				count++;
				HardMaze12();
			}

}

void HardMaze20()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\   *   /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");


	printf("\nMovement Options: Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'w')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze13();
	}

}

void HardMaze21()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /         *   /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze24();
	}
	else
		if (command == 'd')
		{
			count++;
			HardMaze18();
		}

}

void HardMaze22()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\   *                 /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'd')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze26();
	}

}

void HardMaze23()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                 *   /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze26();
	}
	else
		if (command == 's')
		{
			count++;
			HardMaze30();
		}

}

void HardMaze24()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /         *         \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up, Move Right, Move Down (diagonally right)\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze27();
	}
	else
		if (command == 'd')
		{
			count++;
			HardMaze21();
		}
		else 
			if (command == 's')
			{
				count++;
				HardMaze28();
			}
			else 
				if (command == 'w')
				{
					count++;
					HardMaze17();
				}

}

void HardMaze25()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\   *                 /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'w' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'w')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze18();
	}
	else
		if (command == 'd')
		{
			count++;
			HardMaze29();
		}

}

void HardMaze26()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                 *   /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down (diagonally left), Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd' && command != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze22();
	}
	else
		if (command == 'd')
		{
			count++;
			HardMaze23();
		}
		else 
			if (command == 's')
			{
				count++;
				HardMaze29();
			}

}

void HardMaze27()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\   *         \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Up (diagonally), Move Down (diagonally)\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'w' && command != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'w')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze24();
	}
	else
		if (command == 's')
		{
			count++;
			HardMaze31();
		}

}

void HardMaze28()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\   *   /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze24();
	}

}

void HardMaze29()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\   *   /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd' && command != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze25();
	}
	else
		if (command == 'd')
		{
			count++;
			HardMaze26();
		}
		else 
			if (command == 's')
			{
				count++;
				HardMaze35();
			}

}

void HardMaze30()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /         *   /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze33();
	}
	else
		if (command == 'w')
		{
			count++;
			HardMaze23();
		}

}

void HardMaze31()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\   *   /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze27();
	}

}

void HardMaze32()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /         *         \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze34();
	}
	else
		if (command == 'd')
		{
			count++;
			HardMaze35();
		}

}

void HardMaze33()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\   *   /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'd')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze30();
	}

}

void HardMaze34()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\   *         \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Up (diagonally), Move Down (diagonally)\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'w' && command != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'w')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze32();
	}
	else
		if (command == 's')
		{
			count++;
			HardMaze36();
		}

}

void HardMaze35()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\   *   /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		count++;
		HardMaze32();
	}
	else
		if (command == 'w')
		{
			count++;
			HardMaze29();
		}

}

void HardMaze36()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\   *   /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nYou have reached the end!\n"); // user has completed task

	if (count <= 10)
		printf("Portal found!\n");
	printf("Your completed the maze in %d steps.\n", count);
	if (count <= 18) // if and elif function to determine wether the user has passed the test or not
		printf("Congratulations, you definitely have a brain!\n\n");
	else
		if (count >= 19)
			printf("I thought you could be useful...it appears I was wrong.\n\n");

}