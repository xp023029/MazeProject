#include <stdio.h>
#include "Mediummaze.h"

int w, a, s, d;
int count = 0;
char command;
int portal;
int cheat = 27023029;

int main()
{
	printf("Welcome to LabRat Industries.\n"); // introduction
	printf("In this experiment, you will be tested on your maze-solving skills.\n");
	printf("The number of moves you make will be recorded and you will pass/fail depending on the result. No pressure :)\n"); // instructions
	printf("The controls are simple.\nType the letter 'w' to move up, 'a' to move left, 's' to move down and 'd' to move right.\n");
	printf("Good luck and remember, every puzzle has an answer.\n");
	printf("\nBefore we begin, if you know the secret cheatcode, please enter it below. If not, type in the the number '24732'\n");
	scanf("%d", &portal);
	if (portal == cheat)
		printf("Cheatcode confirmed.\n");


	MediumMaze();

	return 0;
}


void MediumMaze()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                * /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'w')// if user has typed in 'd', moves * to the right
	{
		count++;
		MediumMaze1();
	}
}

void MediumMaze1()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         / * /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'w' && command != 's') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 's') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze();
	}
	else
		if (command == 'w') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze2();
		}
}

void MediumMaze2()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /   *  \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options:Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'a') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze1();
	}
	else
		if (command == 'd') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze3();
		}
}

void MediumMaze3()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /  *   \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd' && command != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'a') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze4();
	}
	else
		if (command == 'd') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze5();
		}
		else
			if (command == 'w')
			{
				count++;
				MediumMaze2();
			}
}

void MediumMaze4()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  / * /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 's') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze6();
	}
	else
		if (command == 'w') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze3();
		}
}

void MediumMaze5()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   / * /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'w') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze3();
	}
}

void MediumMaze6()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /     *     /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd' && command != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'a') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze7();
	}
	else
		if (command == 'w') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze4();
		}
		else
			if (command == 'd')
			{
				count++;
				MediumMaze19();
			}

}

void MediumMaze7()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           / *         /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");
	
	printf("\nMovement Options: Move Right, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'd') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 's') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze8();
	}
	else
		if (command == 'd') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze6();
		}

}

void MediumMaze8()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        / * /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 's') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze9();
	}
	else
		if (command == 'w') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze7();
		}

}

void MediumMaze9()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     / *         /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'w' && command != 'd') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 's') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze10();
	}
	else
		if (command == 'w') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze8();
		}
		else
			if (command == 'd')
			{
				count++;
				MediumMaze12();
			}
}

void MediumMaze10()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  / *     /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'd' && command != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'd') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze11();
	}
	else
		if (command == 'w') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze9();
		}

}

void MediumMaze11()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /     * /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'a') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze10();
	}

}

void MediumMaze12()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /     *     /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'd' && command != 'w' && command != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'a') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze9();
	}
	else
		if (command == 'w') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze13();
		}
		else
			if (command == 'd')
			{
				count++;
				MediumMaze14();
			}

}

void MediumMaze13()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   / * /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 's') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze12();
	}

}

void MediumMaze14()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /         * /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 's') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze15();
	}
	else
		if (command == 'a') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze12();
		}

}

void MediumMaze15()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /  *                  /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'd' && command != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'd') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze16();
	}
	else
		if (command == 'w') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze14();
		}

}

void MediumMaze16()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /        *            /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'd' && command != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'd') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze17();
	}
	else
		if (command == 'a') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze15();
		}

}

void MediumMaze17()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /             *       /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'd' && command != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'd') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze18();
	}
	else
		if (command == 'a') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze16();
		}

}

void MediumMaze18()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                   * /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nYou have reached the end!\n"); // user has completed task

	printf("You completed the maze in %d steps.\n", count);
	if (count <= 10)
		printf("Portal found! ");

	if (count <= 16) // if and elif function to determine wether the user has passed the test or not
		printf("Congratulations, you definitely have a brain!\n\n");
	else
		if (count >= 17)
			printf("I thought you could be useful...it appears I was wrong.\n\n");
}

void MediumMaze19()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /         * /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 's') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze20();
	}
	else
		if (command == 'a') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze6();
		}

}

void MediumMaze20()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   / *                \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	if (portal == 27023029)
		printf("You could also try moving down again.\n");
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	if (portal == 27023029)
	{
		while (command != 'w' && command != 'd' && command != 's') // error identifier for directions. only allows 'a' or 's' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			scanf("%s", &command);
		}
		if (command == 'w')// if user has typed in 'd', moves * to the right
		{
			count;
			MediumMaze19();
		}
		else
			if (command == 'd')
			{
				count++;
				MediumMaze21();
			}
			else
				if (command == 's')
				{
					count++;
					MediumMaze18();
				}
	}
	else
	{
		while (command != 'w' && command != 'd') // error identifier for directions. only allows 'd' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			printf("In which direction would you like to go?\n");
			scanf("%s", &command);
		}

		if (command == 'd')// if user has typed in 'd', moves * to the right
		{
			count++;
			MediumMaze21();
		}
		else
			if (command == 'w')// if user has typed in 'd', moves * to the right
			{
				count++;
				MediumMaze19();
			}
	}
}

void MediumMaze21()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /       *          \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'd' && command != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'd') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze22();
	}
	else
		if (command == 'a') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze20();
		}

}

void MediumMaze22()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /            *     \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Right, Move Down, Move up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	if (command == 'd') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze25();
	}
	else
		if (command == 'a') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze21();
		}
		else
			if (command == 's')
			{
				count++;
				MediumMaze23();
			}
			else
				if (command == 'w')
				{
					count++;
					MediumMaze29();
				}

}

void MediumMaze23()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /        *  /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'w' && command != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'w') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze22();
	}
	else
		if (command == 'a') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze24();
		}

}

void MediumMaze24()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /   *       /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'd') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'd') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze23();
	}

}

void MediumMaze25()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                * \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 's') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze26();
	}
	else
		if (command == 'a') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze22();
		}

}

void MediumMaze26()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /  *     \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'd' && command != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'd') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze27();
	}
	else
		if (command == 'w') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze25();
		}

}

void MediumMaze27()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /      * \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 's') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze28();
	}
	else
		if (command == 'a') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze26();
		}

}

void MediumMaze28()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /    *   \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'w') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze27();
	}

}

void MediumMaze29()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /      * \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 's') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze22();
	}
	else
		if (command == 'a') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze30();
		}

}

void MediumMaze30()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /  *     \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'd' && command != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 'd') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze29();
	}
	else
		if (command == 'w') // if user typed in d, moves * to the right
		{
			count++;
			MediumMaze31();
		}

}

void MediumMaze31()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/ *  \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &command);
	}

	if (command == 's') // if user typed in a, moves * to the left
	{
		count++;
		MediumMaze30();
	}

}
