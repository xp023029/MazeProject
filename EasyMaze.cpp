#include <stdio.h>
#include "EasyMaze.h"

void EasyMaze()
{
	printf("\nBefore we begin, if you know the secret cheatcode, please enter it below. If not, type in the the number '24732'\n");
	scanf("%d", &easyportal);
	if (easyportal == easycheat)
		printf("Cheatcode confirmed.\n");

	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|   *                   |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 's' && easycommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 'd')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze1();
	}
	else
		if (easycommand == 's')
		{
			easycounter++;
			EasyMaze4();
		}

}

void EasyMaze1()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|           *           |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 'a' && easycommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 'd')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze2();
	}
	else
		if (easycommand == 'a')
		{
			easycounter++;
			EasyMaze();
		}

}

void EasyMaze2()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                   *   |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 'a')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze1();
	}

}

void EasyMaze3()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |   *   |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 's')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze7();
	}

}

void EasyMaze4()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|   *   |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 's' && easycommand != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 'w')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze();
	}
	else
		if (easycommand == 's')
		{
			easycounter++;
			EasyMaze8();
		}

}

void EasyMaze5()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |   *                   |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 's' && easycommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 'd')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze6();
	}
	else
		if (easycommand == 's')
		{
			easycounter++;
			EasyMaze9();
		}

}

void EasyMaze6()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |           *           |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 's' && easycommand != 'd' && easycommand != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 'd')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze7();
	}
	else
		if (easycommand == 's')
		{
			easycounter++;
			EasyMaze10();
		}
		else
			if (easycommand == 'a')
			{
				easycounter++;
				EasyMaze5();
			}

}

void EasyMaze7()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                   *   |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Up, Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 'a' && easycommand != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 'a')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze6();
	}
	else
		if (easycommand == 'w')
		{
			easycounter++;
			EasyMaze3();
		}

}

void EasyMaze8()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|   *           |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Up, Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 's' && easycommand != 'd' && easycommand != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 'd')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze9();
	}
	else
		if (easycommand == 's')
		{
			easycounter++;
			EasyMaze12();
		}
		else
			if (easycommand == 'w')
			{
				easycounter++;
				EasyMaze4();
			}

}

void EasyMaze9()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|           *   |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Up, Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 'w' && easycommand != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 'w')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze5();
	}
	else
		if (easycommand == 'a')
		{
			easycounter++;
			EasyMaze8();
		}

}

void EasyMaze10()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |   *           |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 'w' && easycommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 'd')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze11();
	}
	else
		if (easycommand == 'w')
		{
			easycounter++;
			EasyMaze6();
		}

}

void EasyMaze11()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |           *   |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 's' && easycommand != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 'a')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze10();
	}
	else
		if (easycommand == 's')
		{
			easycounter++;
			EasyMaze15();
		}

}

void EasyMaze12()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|   *                   |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 'w' && easycommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 'd')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze13();
	}
	else
		if (easycommand == 'w')
		{
			easycounter++;
			EasyMaze8();
		}

}

void EasyMaze13()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|           *           |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	while (easycommand != 'a' && easycommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &easycommand);
	}

	if (easycommand == 'd')// if user has typed in 'd', moves * to the right
	{
		easycounter++;
		EasyMaze14();
	}
	else
		if (easycommand == 'a')
		{
			easycounter++;
			EasyMaze12();
		}

}

void EasyMaze14()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                   *   |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Right\n"); // states clearly the direction options
	if (easyportal == 27023029)
		printf("You could also try moving right again.\n");
	printf("In which direction would you like to go?\n");
	scanf("%s", &easycommand);

	if (easyportal == 27023029)
	{
		while (easycommand != 'a' && easycommand != 'd') // error identifier for directions. only allows 'a' or 's' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			scanf("%s", &easycommand);
		}
		if (easycommand == 'a')// if user has typed in 'd', moves * to the right
		{
			easycounter++;
			EasyMaze13();
		}
		else
			if (easycommand == 'd')
			{
				easycounter++;
				EasyMaze15();
			}
	}
	else
	{
		while (easycommand != 'a') // error identifier for directions. only allows 'd' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			printf("In which direction would you like to go?\n");
			scanf("%s", &easycommand);
		}

		if (easycommand == 'a')// if user has typed in 'd', moves * to the right
		{
			easycounter++;
			EasyMaze13();
		}
	}
}

void EasyMaze15()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |   *   |\n");
	printf("|_______________________|    End|\n");

	printf("\nYou have reached the end!\n"); // user has completed task
	printf("You completed the maze in %d steps.\n", easycounter);
	if (easycounter <= 6)
		printf("Portal found! ");

	if (easycounter <= 9) // if and elif function to determine wether the user has passed the test or not
		printf("Congratulations, you definitely have a brain!\n\n");
	else
		if (easycounter >= 10)
			printf("I thought you could be useful...it appears I was wrong.\n\n");

}