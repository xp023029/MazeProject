#include <stdio.h>
#include "TutorialMaze.cpp"
#include "EasyMaze.cpp"
#include "MediumMaze.cpp"
#include "HardMaze.cpp"

char gamemode;

int main()
{
	printf("Welcome to LabRat Industries.\n"); // introduction
	printf("In this experiment, you will be tested on your maze-solving skills.\n");
	printf("The number of moves you make will be recorded and you will pass/fail depending on the result. No pressure :)\n"); // instructions
	printf("Good luck and remember, every puzzle has an answer.\n");
	printf("Please choose your mode: type t for tutorial, e for easy, m for medium or h for hard");
	scanf("%s", &gamemode);
	printf("\n\t\t\t Let's begin!\n"); // instructions
	printf("The controls are simple.\nType the letter 'w' to move up, 'a' to move left, 's' to move down and 'd' to move right.\n");
	if (gamemode == 't')
		OriginalMaze();
	else
		if (gamemode == 'e')
			EasyMaze();
		else
			if (gamemode == 'm')
				MediumMaze();
			else
				if (gamemode == 'h')
					HardMaze();

	return 0;
}