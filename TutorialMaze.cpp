#include <stdio.h>
#include "TutorialMaze.h"

void OriginalMaze()
{
	printf("Before we begin, if you know the secret cheatcode, please enter it below. If not, type in the word 'cake'\n");
	scanf("%s", &tutorialportal);
	if (tutorialportal == 27023029)
		printf("Cheatcode confirmed.\n");


	printf(" ____________________\n"); // current maze image
	printf(" Start               |\n"); // Lab Rat at the start
	printf("    *                |\n");
	printf(" _____________       |\n");
	printf("|             |      |\n");
	printf("|             |      |\n");
	printf("|             |      |\n");
	printf(" End   |             |\n");
	printf("       |             |\n");
	printf(" ______|_____________|\n");
	printf("\nMovement Options: Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &tutorialcommand);

	while (tutorialcommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &tutorialcommand);
	}

	if (tutorialcommand == 'd')// if user has typed in 'd', moves * to the right
	{
		tutorialcounter++;
		Maze1();
	}
}

void Maze1()
{
	printf(" ____________________\n"); // current maze image
	printf(" Start               |\n"); // Lab Rat at the start
	printf("           *         |\n");
	printf(" _____________       |\n");
	printf("|             |      |\n");
	printf("|             |      |\n");
	printf("|             |      |\n");
	printf(" End   |             |\n");
	printf("       |             |\n");
	printf(" ______|_____________|\n");
	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &tutorialcommand);

	while (tutorialcommand != 'a' && tutorialcommand != 'd') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &tutorialcommand);
	}

	if (tutorialcommand == 'a') // if user typed in a, moves * to the left
	{
		tutorialcounter++;
		OriginalMaze();
	}
	else
		if (tutorialcommand == 'd') // if user typed in d, moves * to the right
		{
			tutorialcounter++;
			Maze2();
		}
}

void Maze2()
{
	printf(" ____________________\n"); // current maze image
	printf(" Start               |\n");
	printf("                 *   |\n");
	printf(" _____________       |\n");
	printf("|             |      |\n");
	printf("|             |      |\n");
	printf("|             |      |\n");
	printf(" End   |             |\n");
	printf("       |             |\n");
	printf(" ______|_____________|\n");
	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options

	if (tutorialportal == 27023029)
		printf("You could also try moving right again.");
	printf("In which direction would you like to go?\n");
	scanf("%s", &tutorialcommand);

	if (tutorialportal == 27023029)
	{
		while (tutorialcommand != 'a' && tutorialcommand != 's' && tutorialcommand != 'd') // error identifier for directions. only allows 'a' or 's' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			scanf("%s", &tutorialcommand);
		}

		if (tutorialcommand == 'a') // if user typed in a, moves * to the left
		{
			tutorialcounter++;
			Maze1();
		}
		else
			if (tutorialcommand == 's') // if user typed in s, moves * down
			{
				tutorialcounter++;
				Maze3();
			}
			else
				if (tutorialcommand == 'd')
				{
					tutorialcounter++;
					Maze8;
				}
	}
	else
	{
		while (tutorialcommand != 'a' && tutorialcommand != 's') // error identifier for directions. only allows 'a' or 's' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			scanf("%s", &tutorialcommand);
		}

		if (tutorialcommand == 'a') // if user typed in a, moves * to the left
		{
			tutorialcounter++;
			Maze1();
		}
		else
			if (tutorialcommand == 's') // if user typed in s, moves * down
			{
				tutorialcounter++;
				Maze3();
			}
	}
}

void Maze3()
{
	printf(" ____________________\n"); // current maze image
	printf(" Start               |\n");
	printf("                     |\n");
	printf(" _____________       |\n");
	printf("|             |      |\n");
	printf("|             |  *   |\n");
	printf("|             |      |\n");
	printf(" End   |             |\n");
	printf("       |             |\n");
	printf(" ______|_____________|\n");
	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &tutorialcommand);

	while (tutorialcommand != 'w' && tutorialcommand != 's') // error identifier for directions. only allows 'w' or 's' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &tutorialcommand);
	}

	if (tutorialcommand == 'w') // if user typed in w, moves * up
	{
		tutorialcounter++;
		Maze2();
	}
	else
		if (tutorialcommand == 's') // if user typed in a, moves * down
		{
			tutorialcounter++;
			Maze4();
		}
}

void Maze4()
{
	printf(" ____________________\n"); // current maze image
	printf(" Start               |\n");
	printf("                     |\n");
	printf(" _____________       |\n");
	printf("|             |      |\n");
	printf("|             |      |\n");
	printf("|             |      |\n");
	printf(" End   |             |\n");
	printf("       |         *   |\n");
	printf(" ______|_____________|\n");
	printf("\nMovement Options: Move Up, Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &tutorialcommand);

	while (tutorialcommand != 'w' && tutorialcommand != 'a') // error identifier for directions. only allows 'w' or 'a' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &tutorialcommand);
	}

	if (tutorialcommand == 'w') // if user typed in w, moves * up
	{
		tutorialcounter++;
		Maze3();
	}
	else
		if (tutorialcommand == 'a') // if user typed in a, moves * go to the left
		{
			tutorialcounter++;
			Maze5();
		}
}

void Maze5()
{
	printf(" ____________________\n"); // current maze image
	printf(" Start               |\n");
	printf("                     |\n");
	printf(" _____________       |\n");
	printf("|             |      |\n");
	printf("|             |      |\n");
	printf("|             |      |\n");
	printf(" End   |             |\n");
	printf("       |   *         |\n");
	printf(" ______|_____________|\n");
	printf("\nMovement Options: Move Right, Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &tutorialcommand);

	while (tutorialcommand != 'd' && tutorialcommand != 'w') // error identifier for directions. only allows 'd' or 'w' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &tutorialcommand);
	}

	if (tutorialcommand == 'd') // if user typed in d, moves * to the right
	{
		tutorialcounter++;
		Maze4();
	}
	else
		if (tutorialcommand == 'w') // if user typed in w, moves * up
		{
			tutorialcounter++;
			Maze6();
		}
}

void Maze6()
{
	printf(" ____________________\n"); // current maze image
	printf(" Start               |\n");
	printf("                     |\n");
	printf(" _____________       |\n");
	printf("|             |      |\n");
	printf("|         *   |      |\n");
	printf("|             |      |\n");
	printf(" End   |             |\n");
	printf("       |             |\n");
	printf(" ______|_____________|\n");
	printf("\nMovement Options: Move Down, Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &tutorialcommand);

	while (tutorialcommand != 's' && tutorialcommand != 'a') // error identifier for directions. only allows 's' or 'a' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &tutorialcommand);
	}

	if (tutorialcommand == 's') // if user typed in s, moves * down
	{
		tutorialcounter++;
		Maze5();
	}
	else
		if (tutorialcommand == 'a') // if user typed in a, moves * to the left
		{
			tutorialcounter++;
			Maze7();
		}
}

void Maze7()
{
	printf(" ____________________\n"); // current maze image
	printf(" Start               |\n");
	printf("                     |\n");
	printf(" _____________       |\n");
	printf("|             |      |\n");
	printf("|   *         |      |\n");
	printf("|             |      |\n");
	printf(" End   |             |\n");
	printf("       |             |\n");
	printf(" ______|_____________|\n");
	printf("\nMovement Options: Move Right, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &tutorialcommand);
	while (tutorialcommand != 'd' && tutorialcommand != 's') // error identifier for directions. only allows 'd' or 's' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &tutorialcommand);
	}

	if (tutorialcommand == 'd') // if user typed in a, moves * to the right
	{
		tutorialcounter++;
		Maze6();
	}
	else
		if (tutorialcommand == 's') // if user typed in s, moves * down
		{
			tutorialcounter++;
			Maze8();
		}
}

void Maze8()
{
	printf(" ____________________\n"); // current maze image
	printf(" Start               |\n"); // final maze image
	printf("                     |\n");
	printf(" _____________       |\n");
	printf("|             |      |\n");
	printf("|             |      |\n");
	printf("|             |      |\n");
	printf(" End   |             |\n");
	printf("   *   |             |\n");
	printf(" ______|_____________|\n");
	printf("\nYou have reached the end!\n"); // user has completed task

	if (tutorialcounter <= 4)
		printf("Portal found!");
	printf("You completed the maze in %d steps.\n", tutorialcounter);
	if (tutorialcounter <= 8) // if and elif function to determine wether the user has passed the test or not
		printf("Congratulations, you definitely have a brain! Now, try something that requires some thinking.\n\n");
	else
		if (tutorialcounter >= 9)
			printf("I thought you could be useful...it appears I was wrong.\n\n");

}