#include <stdio.h>
#include<stdio.h>
#include "HardMaze.h"

void HardMaze()
{
	printf("\nBefore we begin, if you know the secret cheatcode, please enter it below. If not, type in the the number '24732'\n");
	scanf("%d", &hardportal);
	if (hardportal == hardcheat)
		printf("Cheatcode confirmed.\n");

	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /         *   /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze1();
	}

}

void HardMaze1()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /         *                       \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'w' && hardcommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze3();
	}
	else
		if (hardcommand == 'w')
		{
			hardcounter++;
			HardMaze();
		}
		else
			if (hardcommand == 'd')
			{
				hardcounter++;
				HardMaze4();
			}

}

void HardMaze2()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                       *         \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze4();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze5();
		}

}

void HardMaze3()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /         *   /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'd' && hardcommand != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze6();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze1();
		}
		else
			if (hardcommand == 's')
			{
				hardcounter++;
				HardMaze10();
			}

}

void HardMaze4()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\   *   /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze1();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze2();
		}

}

void HardMaze5()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\   *   /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze2();
	}
	else
		if (hardcommand == 's')
		{
			hardcounter++;
			HardMaze12();
		}

}

void HardMaze6()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\   *   /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'd')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze3();
	}

}

void HardMaze7()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /         *   /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze10();
	}
	else
		if (hardcommand == 's')
		{
			hardcounter++;
			HardMaze14();
		}

}

void HardMaze8()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\   *   /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 's')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze15();
	}

}

void HardMaze9()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\   *   /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 's')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze16();
	}

}

void HardMaze10()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\   *   /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'w' && hardcommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'w')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze3();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze7();
		}

}

void HardMaze11()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\   *         \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n         ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Right\n"); // states clearly the direction options
	if (hardportal == 27023029)
		printf("You could also try moving down.\n");
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	if (hardportal == 27023029)
	{
		while (hardcommand != 's' && hardcommand != 'd') // error identifier for directions. only allows 'a' or 's' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			scanf("%s", &hardcommand);
		}
		if (hardcommand == 's')// if user has typed in 'd', moves * to the right
		{
			hardcounter++;
			HardMaze36();
		}
		else
			if (hardcommand == 'd')
			{
				hardcounter++;
				HardMaze15();
			}
	}
	else
	{
		while (hardcommand != 'd') // error identifier for directions. only allows 'd' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			printf("In which direction would you like to go?\n");
			scanf("%s", &hardcommand);
		}

		if (hardcommand == 'd')// if user has typed in 'd', moves * to the right
		{
			hardcounter++;
			HardMaze15();
		}
	}
}

void HardMaze12()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\   *   /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 's' && hardcommand != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 's')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze19();
	}
	else
		if (hardcommand == 'w')
		{
			hardcounter++;
			HardMaze5();
		}

}

void HardMaze13()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\   *                 /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 's' && hardcommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 's')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze20();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze17();
		}

}

void HardMaze14()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                 *   /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze17();
	}
	else
		if (hardcommand == 'w')
		{
			hardcounter++;
			HardMaze7();
		}

}

void HardMaze15()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\   *                 /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'd' && hardcommand != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze11();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze19();
		}
		else
			if (hardcommand == 'w')
			{
				hardcounter++;
				HardMaze12();
			}

}

void HardMaze16()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                 *   /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze19();
	}
	else
		if (hardcommand == 'w')
		{
			hardcounter++;
			HardMaze9();
		}

}

void HardMaze17()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\   *   /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'd' && hardcommand != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze13();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze14();
		}
		else
			if (hardcommand == 's')
			{
				hardcounter++;
				HardMaze24();
			}

}

void HardMaze18()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /         *   /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze21();
	}
	else
		if (hardcommand == 's')
		{
			hardcounter++;
			HardMaze25();
		}

}

void HardMaze19()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\   *   /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'd' && hardcommand != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze15();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze16();
		}
		else
			if (hardcommand == 'w')
			{
				hardcounter++;
				HardMaze12();
			}

}

void HardMaze20()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\   *   /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");


	printf("\nMovement Options: Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'w')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze13();
	}

}

void HardMaze21()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /         *   /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze24();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze18();
		}

}

void HardMaze22()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\   *                 /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'd')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze26();
	}

}

void HardMaze23()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                 *   /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze26();
	}
	else
		if (hardcommand == 's')
		{
			hardcounter++;
			HardMaze30();
		}

}

void HardMaze24()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /         *         \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up, Move Right, Move Down (diagonally right)\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze27();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze21();
		}
		else
			if (hardcommand == 's')
			{
				hardcounter++;
				HardMaze28();
			}
			else
				if (hardcommand == 'w')
				{
					hardcounter++;
					HardMaze17();
				}

}

void HardMaze25()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\   *                 /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'w' && hardcommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'w')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze18();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze29();
		}

}

void HardMaze26()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                 *   /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down (diagonally left), Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'd' && hardcommand != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze22();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze23();
		}
		else
			if (hardcommand == 's')
			{
				hardcounter++;
				HardMaze29();
			}

}

void HardMaze27()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\   *         \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Up (diagonally), Move Down (diagonally)\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'w' && hardcommand != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'w')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze24();
	}
	else
		if (hardcommand == 's')
		{
			hardcounter++;
			HardMaze31();
		}

}

void HardMaze28()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\   *   /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze24();
	}

}

void HardMaze29()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\   *   /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'd' && hardcommand != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze25();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze26();
		}
		else
			if (hardcommand == 's')
			{
				hardcounter++;
				HardMaze35();
			}

}

void HardMaze30()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /         *   /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze33();
	}
	else
		if (hardcommand == 'w')
		{
			hardcounter++;
			HardMaze23();
		}

}

void HardMaze31()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\   *   /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze27();
	}

}

void HardMaze32()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /         *         \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze34();
	}
	else
		if (hardcommand == 'd')
		{
			hardcounter++;
			HardMaze35();
		}

}

void HardMaze33()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\   *   /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'd')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze30();
	}

}

void HardMaze34()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\   *         \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Up (diagonally), Move Down (diagonally)\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'w' && hardcommand != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'w')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze32();
	}
	else
		if (hardcommand == 's')
		{
			hardcounter++;
			HardMaze36();
		}

}

void HardMaze35()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\   *   /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\       /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nMovement Options: Move Left, Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &hardcommand);

	while (hardcommand != 'a' && hardcommand != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &hardcommand);
	}

	if (hardcommand == 'a')// if user has typed in 'd', moves * to the right
	{
		hardcounter++;
		HardMaze32();
	}
	else
		if (hardcommand == 'w')
		{
			hardcounter++;
			HardMaze29();
		}

}

void HardMaze36()
{
	printf("                                                                          \n    ");
	printf("                                   /     \\                                 \n   ");
	printf("                              _____/       \\_____                     \n         ");
	printf("                       /             /     \\                      \n      ");
	printf("                    _____/        _____/       \\_____                     \n      ");
	printf("                   /                                 \\               \n       ");
	printf("            _____/        _____         _____        \\_____        \n             ");
	printf("     /             /     \\       /     \\       /     \\     \n           ");
	printf("      /             /       \\_____/       \\     /       \\    \n           ");
	printf("      \\       /             /     \\       /     \\       /    \n           ");
	printf("       \\_____/             /       \\     /       \\     /         \n           ");
	printf("       /     \\       /     \\             \\       /     \\      \n           ");
	printf("      /       \\_____/       \\_____        \\     /       \\          \n          ");
	printf("       \\                     /     \\                     /     \n         ");
	printf("         \\              _____/       \\_____         _____/         \n       ");
	printf("           /     \\       /             /     \\       /     \\      \n           ");
	printf("      /       \\     /             /       \\_____/       \\          \n          ");
	printf("       \\       /             /     \\                     /     \n         ");
	printf("         \\_____/        _____/       \\_____              /         \n       ");
	printf("           /                   \\                     /     \\      \n           ");
	printf("      /        _____        \\_____         _____/       \\          \n          ");
	printf("       \\             \\       /     \\       /             /     \n         ");
	printf("         \\_____        \\_____/       \\     /        _____/         \n       ");
	printf("                 \\       /                   \\       /           \n       ");
	printf("                  \\_____/        _____        \\_____/         \n             ");
	printf("                  \\             \\       /        \n     ");
	printf("                           \\_____        \\_____/          \n         ");
	printf("                             \\   *   /                 \n      ");
	printf("                                 \\     /                    \n         ");

	printf("\nYou have reached the end!\n"); // user has completed task

	if (hardcounter <= 10)
		printf("You have found the secret portal");
	printf("Your completed the maze in %d steps.\n", hardcounter);
	if (hardcounter <= 18) // if and elif function to determine wether the user has passed the test or not
		printf("Congratulations, you definitely have a brain!\n\n");
	else
		if (hardcounter >= 19)
			printf("I thought you could be useful...it appears I was wrong.\n\n");

}