#include <stdio.h>
#include "Easymaze.h"

int counter = 0; 
char command;
int easyportal;
int cheatcode = 27023029; //student number


int main()
{
	printf("Welcome to LabRat Industries.\n"); // introduction
	printf("In this experiment, you will be tested on your maze-solving skills.\n");
	printf("The number of moves you make will be recorded and you will pass/fail depending on the result. No pressure :)\n"); // instructions
	printf("The controls are simple.\nType the letter 'w' to move up, 'a' to move left, 's' to move down and 'd' to move right.\n");
	printf("Then hit enter to submit your choice.\n");

	printf("Good luck and remember, every puzzle has an answer.\n");
	
	EasyMaze();

	return 0;
}


void EasyMaze()
{
	printf("\nBefore we begin, if you know the secret cheatcode, please enter it below. If not, type in the the number '24732'\n");
	scanf("%d", &easyportal);
	if (easyportal == cheatcode)
		printf("Cheatcode confirmed.\n");

	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|   *                   |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'd')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze1();
	}
	else
		if (command == 's')
		{
			counter++;
			EasyMaze4();
		}

}

void EasyMaze1()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|           *           |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'd')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze2();
	}
	else
		if (command == 'a')
		{
			counter++;
			EasyMaze();
		}

}

void EasyMaze2()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                   *   |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze1();
	}

}

void EasyMaze3()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |   *   |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 's')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze7();
	}

}

void EasyMaze4()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|   *   |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'w')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze();
	}
	else
		if (command == 's')
		{
			counter++;
			EasyMaze8();
		}

}

void EasyMaze5()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |   *                   |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'd')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze6();
	}
	else
		if (command == 's')
		{
			counter++;
			EasyMaze9();
		}

}

void EasyMaze6()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |           *           |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'd' && command != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'd')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze7();
	}
	else
		if (command == 's')
		{
			counter++;
			EasyMaze10();
		}
		else
			if (command == 'a')
			{
				counter++;
				EasyMaze5();
			}

}

void EasyMaze7()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                   *   |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Up, Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze6();
	}
	else
		if (command == 'w')
		{
			counter++;
			EasyMaze3();
		}

}

void EasyMaze8()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|   *           |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Up, Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'd' && command != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'd')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze9();
	}
	else
		if (command == 's')
		{
			counter++;
			EasyMaze12();
		}
		else
			if (command == 'w')
			{
				counter++;
				EasyMaze4();
			}

}

void EasyMaze9()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|           *   |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Up, Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'w' && command != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'w')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze5();
	}
	else
		if (command == 'a')
		{
			counter++;
			EasyMaze8();
		}

}

void EasyMaze10()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |   *           |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'w' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'd')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze11();
	}
	else
		if (command == 'w')
		{
			counter++;
			EasyMaze6();
		}

}

void EasyMaze11()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |           *   |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 's' && command != 'a') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'a')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze10();
	}
	else
		if (command == 's')
		{
			counter++;
			EasyMaze15();
		}

}

void EasyMaze12()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|   *                   |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'w' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'd')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze13();
	}
	else
		if (command == 'w')
		{
			counter++;
			EasyMaze8();
		}

}

void EasyMaze13()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|           *           |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	while (command != 'a' && command != 'd') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &command);
	}

	if (command == 'd')// if user has typed in 'd', moves * to the right
	{
		counter++;
		EasyMaze14();
	}
	else
		if (command == 'a')
		{
			counter++;
			EasyMaze12();
		}

}

void EasyMaze14()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                   *   |       |\n");
	printf("|_______________________|    End|\n");

	printf("\nMovement Options: Move Down, Move Right\n"); // states clearly the direction options
	if (easyportal == 27023029)
		printf("You could also try moving right again.\n");
	printf("In which direction would you like to go?\n");
	scanf("%s", &command);

	if (easyportal == 27023029)
	{
		while (command != 'a' && command != 'd') // error identifier for directions. only allows 'a' or 's' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			scanf("%s", &command);
		}
		if (command == 'a')// if user has typed in 'd', moves * to the right
		{
			counter++;
			EasyMaze13();
		}
		else 
			if (command == 'd')
			{
				counter++;
				EasyMaze15();
			}
	}
	else
	{
		while (command != 'a') // error identifier for directions. only allows 'd' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			printf("In which direction would you like to go?\n");
			scanf("%s", &command);
		}
	
		if (command == 'a')// if user has typed in 'd', moves * to the right
		{
			counter++;
			EasyMaze13();
		}
	}
}

void EasyMaze15()
{
	printf("         _______________________ \n");
	printf("|Start                  |       |\n");
	printf("|                       |       |\n");
	printf("|        _______________|       |\n");
	printf("|       |                       |\n");
	printf("|       |                       |\n");
	printf("|       |                _______|\n");
	printf("|               |               |\n");
	printf("|               |               |\n");
	printf("|        _______|_______        |\n");
	printf("|                       |       |\n");
	printf("|                       |   *   |\n");
	printf("|_______________________|    End|\n");

	printf("\nYou have reached the end!\n"); // user has completed task
	printf("You completed the maze in %d steps.\n", counter);
	if (counter <= 6)
		printf("Portal found!\n");
	
	if (counter <= 9) // if and elif function to determine wether the user has passed the test or not
		printf("Congratulations, you definitely have a brain!\n\n");
	else
		if (counter >= 10)
			printf("I thought you could be useful...it appears I was wrong.\n\n");

}
