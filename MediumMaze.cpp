#include <stdio.h>
#include "MediumMaze.h"

void MediumMaze()
{
	printf("\nBefore we begin, if you know the secret cheatcode, please enter it below. If not, type in the the number '24732'\n");
	scanf("%d", &mediumportal);
	if (mediumportal == medcheat)
		printf("Cheatcode confirmed.\n");

	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                * /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'w') // error identifier for directions. only allows 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		printf("In which direction would you like to go?\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'w')// if user has typed in 'd', moves * to the right
	{
		mediumcounter++;
		MediumMaze1();
	}
}

void MediumMaze1()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         / * /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'w' && mediumcommand != 's') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 's') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze();
	}
	else
		if (mediumcommand == 'w') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze2();
		}
}

void MediumMaze2()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /   *  \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options:Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'a' && mediumcommand != 'd') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'a') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze1();
	}
	else
		if (mediumcommand == 'd') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze3();
		}
}

void MediumMaze3()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /  *   \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'a' && mediumcommand != 'd' && mediumcommand != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'a') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze4();
	}
	else
		if (mediumcommand == 'd') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze5();
		}
		else
			if (mediumcommand == 'w')
			{
				mediumcounter++;
				MediumMaze2();
			}
}

void MediumMaze4()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  / * /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 's' && mediumcommand != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 's') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze6();
	}
	else
		if (mediumcommand == 'w') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze3();
		}
}

void MediumMaze5()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   / * /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'w') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze3();
	}
}

void MediumMaze6()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /     *     /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'a' && mediumcommand != 'd' && mediumcommand != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'a') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze7();
	}
	else
		if (mediumcommand == 'w') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze4();
		}
		else
			if (mediumcommand == 'd')
			{
				mediumcounter++;
				MediumMaze19();
			}

}

void MediumMaze7()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           / *         /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Right, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 's' && mediumcommand != 'd') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 's') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze8();
	}
	else
		if (mediumcommand == 'd') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze6();
		}

}

void MediumMaze8()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        / * /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 's' && mediumcommand != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 's') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze9();
	}
	else
		if (mediumcommand == 'w') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze7();
		}

}

void MediumMaze9()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     / *         /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 's' && mediumcommand != 'w' && mediumcommand != 'd') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 's') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze10();
	}
	else
		if (mediumcommand == 'w') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze8();
		}
		else
			if (mediumcommand == 'd')
			{
				mediumcounter++;
				MediumMaze12();
			}
}

void MediumMaze10()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  / *     /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'd' && mediumcommand != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'd') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze11();
	}
	else
		if (mediumcommand == 'w') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze9();
		}

}

void MediumMaze11()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /     * /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'a') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze10();
	}

}

void MediumMaze12()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /     *     /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'd' && mediumcommand != 'w' && mediumcommand != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'a') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze9();
	}
	else
		if (mediumcommand == 'w') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze13();
		}
		else
			if (mediumcommand == 'd')
			{
				mediumcounter++;
				MediumMaze14();
			}

}

void MediumMaze13()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   / * /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 's') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 's') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze12();
	}

}

void MediumMaze14()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /         * /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 's' && mediumcommand != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 's') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze15();
	}
	else
		if (mediumcommand == 'a') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze12();
		}

}

void MediumMaze15()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /  *                  /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'd' && mediumcommand != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'd') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze16();
	}
	else
		if (mediumcommand == 'w') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze14();
		}

}

void MediumMaze16()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /        *            /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'd' && mediumcommand != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'd') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze17();
	}
	else
		if (mediumcommand == 'a') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze15();
		}

}

void MediumMaze17()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /             *       /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'd' && mediumcommand != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'd') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze18();
	}
	else
		if (mediumcommand == 'a') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze16();
		}

}

void MediumMaze18()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                   * /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nYou have reached the end!\n"); // user has completed task

	printf("You completed the maze in %d steps.\n", mediumcounter);
	if (mediumcounter <= 10)
		printf("Portal found! ");

	if (mediumcounter <= 16) // if and elif function to determine wether the user has passed the test or not
		printf("Congratulations, you definitely have a brain!\n\n");
	else
		if (mediumcounter >= 17)
			printf("I thought you could be useful...it appears I was wrong.\n\n");

}

void MediumMaze19()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /         * /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 's' && mediumcommand != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 's') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze20();
	}
	else
		if (mediumcommand == 'a') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze6();
		}

}

void MediumMaze20()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   / *                \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	if (mediumportal == 27023029)
		printf("You could also try moving down again.\n");
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	if (mediumportal == 27023029)
	{
		while (mediumcommand != 'w' && mediumcommand != 'd' && mediumcommand != 's') // error identifier for directions. only allows 'a' or 's' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			scanf("%s", &mediumcommand);
		}
		if (mediumcommand == 'w')// if user has typed in 'd', moves * to the right
		{
			mediumcounter++;
			MediumMaze19();
		}
		else
			if (mediumcommand == 'd')
			{
				mediumcounter++;
				MediumMaze21();
			}
			else 
				if (mediumcommand == 's')
				{
					mediumcounter++;
					MediumMaze18();
				}
	}
	else
	{
		while (mediumcommand != 'w' && mediumcommand != 'd') // error identifier for directions. only allows 'd' to continue
		{
			printf("Invalid direction, did you not read the movement options?");
			printf("\nTry again.\n");
			printf("In which direction would you like to go?\n");
			scanf("%s", &mediumcommand);
		}

		if (mediumcommand == 'd')// if user has typed in 'd', moves * to the right
		{
			mediumcounter++;
			MediumMaze21();
		}
		else 
			if (mediumcommand == 'w')// if user has typed in 'd', moves * to the right
			{
				mediumcounter++;
				MediumMaze19();
			}
	}
}

void MediumMaze21()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /       *          \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'd' && mediumcommand != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'd') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze22();
	}
	else
		if (mediumcommand == 'a') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze20();
		}

}

void MediumMaze22()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /            *     \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Right, Move Down, Move up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	if (mediumcommand == 'd') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze25();
	}
	else
		if (mediumcommand == 'a') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze21();
		}
		else
			if (mediumcommand == 's')
			{
				mediumcounter++;
				MediumMaze23();
			}
			else
				if (mediumcommand == 'w')
				{
					mediumcounter++;
					MediumMaze29();
				}

}

void MediumMaze23()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /        *  /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'w' && mediumcommand != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'w') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze22();
	}
	else
		if (mediumcommand == 'a') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze24();
		}

}

void MediumMaze24()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /   *       /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'd') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'd') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze23();
	}

}

void MediumMaze25()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                * \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 's' && mediumcommand != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 's') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze26();
	}
	else
		if (mediumcommand == 'a') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze22();
		}

}

void MediumMaze26()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /  *     \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'd' && mediumcommand != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'd') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze27();
	}
	else
		if (mediumcommand == 'w') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze25();
		}

}

void MediumMaze27()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /      * \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 's' && mediumcommand != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 's') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze28();
	}
	else
		if (mediumcommand == 'a') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze26();
		}

}

void MediumMaze28()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /    *   \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'w') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze27();
	}

}

void MediumMaze29()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /      * \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Left, Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 's' && mediumcommand != 'a') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 's') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze22();
	}
	else
		if (mediumcommand == 'a') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze30();
		}

}

void MediumMaze30()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/    \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /  *     \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Up, Move Right\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 'd' && mediumcommand != 'w') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 'd') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze29();
	}
	else
		if (mediumcommand == 'w') // if user typed in d, moves * to the right
		{
			mediumcounter++;
			MediumMaze31();
		}

}

void MediumMaze31()
{
	printf("                      /\\                     \n ");
	printf("                    /  \\                    \n ");
	printf("                   /    \\                   \n ");
	printf("                  /      \\                  \n ");
	printf("                 /   /    \\                 \n");
	printf("	         /   /      \\                \n ");
	printf("                   /   /   /\\               \n ");
	printf("                  /   /   /  \\              \n ");
	printf("             ____/   /___/ *  \\             \n ");
	printf("            /           /      \\            \n ");
	printf("           /           /        \\           \n ");
	printf("          /   ____    /_____     \\          \n ");
	printf("         /   /   /                \\         \n ");
	printf("        /   /   /                  \\        \n ");
	printf("       /   /   /_________           \\       \n ");
	printf("      /           /           /      \\      \n ");
	printf("     /           /           /        \\     \n ");
	printf("    /   ____    /___________/_____     \\    \n ");
	printf("   /       /                     /      \\   \n ");
	printf("  /       /                     /        \\   \n");
	printf("  /_______/_______________      /__________\\ \n  ");

	printf("\nMovement Options: Move Down\n"); // states clearly the direction options
	printf("In which direction would you like to go?\n");
	scanf("%s", &mediumcommand);

	while (mediumcommand != 's') // error identifier for directions. only allows 'a' or 'd' to continue
	{
		printf("Invalid direction, did you not read the movement options?");
		printf("\nTry again.\n");
		scanf("%s", &mediumcommand);
	}

	if (mediumcommand == 's') // if user typed in a, moves * to the left
	{
		mediumcounter++;
		MediumMaze30();
	}

}